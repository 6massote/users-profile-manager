package br.com.intpost.usersprofile.unit.util;

import br.com.intpost.usersprofile.domain.model.Profile;
import br.com.intpost.usersprofile.util.MergeBeans;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;

import static org.assertj.core.api.Assertions.assertThat;

public class MergeBeansTest {

    @BeforeClass
    public static void setUpAll() {
        FixtureFactoryLoader.loadTemplates("br.com.intpost.usersprofile.fixture");
    }

    @Test
    public void copyProperties() throws InvocationTargetException, IllegalAccessException {
        final Profile profileOne = Fixture.from(Profile.class).gimme("new");

        final Profile profileTwo = Profile.builder()
            .email("teste@example.com")
            .build();

        new MergeBeans().copyProperties(profileOne, profileTwo);
        assertThat(profileOne.getEmail()).isEqualTo("teste@example.com");
        assertThat(profileOne.getUsername()).isNotNull();

    }

}