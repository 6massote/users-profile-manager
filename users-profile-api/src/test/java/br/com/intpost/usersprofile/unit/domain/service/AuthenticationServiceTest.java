package br.com.intpost.usersprofile.unit.domain.service;

import br.com.intpost.usersprofile.domain.model.Profile;
import br.com.intpost.usersprofile.domain.repository.ProfileDatabaseRepository;
import br.com.intpost.usersprofile.domain.service.AuthenticationService;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.User;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AuthenticationServiceTest {

    @InjectMocks
    private AuthenticationService authenticationService;

    @Mock
    private ProfileDatabaseRepository profileDatabaseRepositoryMock;

    @BeforeClass
    public static void setUpAll() {
        FixtureFactoryLoader.loadTemplates("br.com.intpost.usersprofile.fixture");
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findByUsername() throws Exception {
        final Profile profile = Fixture.from(Profile.class).gimme("new");
        final String username = "gprado";

        when(profileDatabaseRepositoryMock.findByUsername(username)).thenReturn(profile);

        final Optional<User> user = authenticationService.findByUsername(username);

        verify(profileDatabaseRepositoryMock).findByUsername("gprado");

        assertThat(user.isPresent()).isTrue();
    }

    @Test
    public void findByUsernameEmpty() throws Exception {
        when(profileDatabaseRepositoryMock.findByUsername("")).thenReturn(null);

        final Optional<User> user = authenticationService.findByUsername("");

        verify(profileDatabaseRepositoryMock).findByUsername("");

        assertThat(user.isPresent()).isFalse();
    }

}