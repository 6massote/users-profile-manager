package br.com.intpost.usersprofile.controller.filters;

import liquibase.util.StringUtils;
import lombok.Data;

@Data
public class AccountCredentials {

    private String username;
    private String password;

    public boolean isValid() {
        return StringUtils.isNotEmpty(getUsername()) && StringUtils.isNotEmpty(getPassword());
    }

}
