package br.com.intpost.usersprofile.config;

import br.com.intpost.usersprofile.controller.filters.JWTAuthenticationFilter;
import br.com.intpost.usersprofile.controller.filters.JWTLoginFilter;
import br.com.intpost.usersprofile.domain.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.util.Assert;

import java.util.Collection;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AuthenticationService authenticationService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
            .authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers( "/login").permitAll()
            .anyRequest()
                .authenticated()
            .and()
            // We filter the login requests
            .addFilterBefore(
                    new JWTLoginFilter("/login", authenticationManager()),
                    UsernamePasswordAuthenticationFilter.class)
            // And filter other requests to check the presence of JWT in header
            .addFilterBefore(
                    new JWTAuthenticationFilter(),
                    UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.authenticationProvider(new AuthenticationProvider() {

            @Override
            public Authentication authenticate(Authentication authentication) throws AuthenticationException {
                Assert.notNull(authentication, "No authentication data provided");

                final String username = (String) authentication.getPrincipal();
                final String password = (String) authentication.getCredentials();

                final User user = authenticationService
                    .findByUsername(username)
                    .orElseThrow(() -> new UsernameNotFoundException("User not found: " + username));

                // TODO Don't use opened password in production
                if (!password.equals(user.getPassword())) {
                    throw new BadCredentialsException("Authentication Failed. Username or Password not valid.");
                }

                if (user.getAuthorities().stream().noneMatch(role -> role.getAuthority().equals("ADMIN"))) {
                    throw new InsufficientAuthenticationException("User has no ADMIN roles assigned");
                };

                Collection<GrantedAuthority> authorities = user.getAuthorities();

                return new UsernamePasswordAuthenticationToken(username, null, authorities);
            }

            @Override
            public boolean supports(Class<?> authentication) {
                return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
            }
        });
    }
}
