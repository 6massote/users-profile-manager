package br.com.intpost.usersprofile.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Profile implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Size(min=1, max=64)
    @Column(nullable = false)
    private String position;

    @Size(min=1, max=64)
    @Column(nullable = false)
    private String email;

    @Size(min=1, max=64)
    @Column(nullable = false)
    private String username;

    @Size(min=1, max=64)
    @Column(nullable = false)
    private String password;

    @Size(min=1, max=64)
    @Column(nullable = false)
    private String firstName;

    @Size(min=1, max=64)
    @Column(nullable = false)
    private String lastName;

    @Size(min=1, max=24)
    @Column(nullable = false)
    private String postalCode;

    @Size(min=1, max=2056)
    @Column(nullable = true)
    private String about;

    @Size(min=1, max=1024)
    @Column(nullable = true)
    private String image;

    @Size(min=1, max=256)
    @Column(nullable = true)
    private String roles;

}
